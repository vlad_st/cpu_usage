#include <stdlib.h>
#include <sys/types.h>
#include <sys/times.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define INTERVAL	3

struct stats {
	long unsigned int	user;    // user (application) usage
	long unsigned int	nice;    // user usage with "niced" priority
	long unsigned int	system;  // system (kernel) level usage
	long unsigned int	idle;    // CPU idle and no disk I/O outstanding
	long unsigned int	iowait;  // CPU idle but with outstanding disk I/O
	long unsigned int	irq;     // Interrupt requests
	long unsigned int	softirq; // Soft interrupt requests
	long unsigned int	steal;   // Invol wait, hypervisor svcing other virtual CPU
	long unsigned int	total;
};

struct pstat {
	char name[32];
	long unsigned int utime_ticks;
	long int cutime_ticks;
	long unsigned int stime_ticks;
	long int cstime_ticks;
	long unsigned int vsize; // virtual memory size in bytes
	long unsigned int rss; //Resident  Set  Size in bytes
	long unsigned int total_ticks;  // ticks per cpu
};

int get_usage(const pid_t pid, struct pstat* result)
{
	struct stats st;
	char stat_filepath[30] = "/proc/stat";

	int numCPU = sysconf(_SC_NPROCESSORS_ONLN);

	FILE *fpstat = fopen(stat_filepath, "r");
	if (fpstat == NULL) {
		perror("fopen");
		exit(-1);
	}

	if (fseek(fpstat, 0, SEEK_SET) < 0) {
		fclose(fpstat);
		return -1;
	}

	fscanf(fpstat, "cpu %lu %lu %lu %lu %lu %lu %lu %lu",
			&st.user, &st.nice, &st.system, &st.idle,
			&st.iowait, &st.irq, &st.softirq, &st.steal);

	st.total = st.user + st.nice  + st.system + st.idle + st.iowait +
				st.irq  + st.steal + st.softirq;

	fclose(fpstat);

	snprintf(stat_filepath, sizeof stat_filepath, "/proc/%d/stat", pid);

	fpstat = fopen(stat_filepath, "r");
	if (fpstat == NULL) {
		perror("fopen");
		exit(-1);
	}

	bzero(result, sizeof(struct pstat));
	long int rss;
	if (fscanf(fpstat, "%*d %32s %*c %*d %*d %*d %*d %*d %*u %*u %*u %*u %*u %lu"
		"%lu %ld %ld %*d %*d %*d %*d %*u %lu %ld",
		result->name,
		&result->utime_ticks, &result->stime_ticks,
		&result->cutime_ticks, &result->cstime_ticks, &result->vsize,
		&rss) == EOF)
	{
		fclose(fpstat);
		return -1;
	}

	fclose(fpstat);

	result->rss = rss * getpagesize();
	result->total_ticks = st.total/numCPU;

	return 0;
}

void calc_cpu_usage_pct(const struct pstat* cur_usage,
						const struct pstat* last_usage,
						double* usage)
{
	const long unsigned int pid_diff =
		( cur_usage->utime_ticks + cur_usage->stime_ticks ) -
		( last_usage->utime_ticks + last_usage->stime_ticks );

	*usage = (100 * pid_diff)/((double)(cur_usage->total_ticks - last_usage->total_ticks));
}

int main(int argc, char** argv)
{
	struct pstat first, prev, curr;
	double pct;

	struct tms t;
	times( &t );

	if( argc <= 1 ) {
		printf( "please supply a pid\n" );
		return 1;
	}

	if( get_usage(atoi(argv[1]), &first) ) {
			printf( "error\n" );
	}

	while( 1 )
	{
		if( get_usage(atoi(argv[1]), &prev) ) {
			printf( "error\n" );
		}

		sleep( INTERVAL );

		if( get_usage(atoi(argv[1]), &curr) ) {
			printf( "error\n" );
		}

		calc_cpu_usage_pct(&curr, &prev, &pct);

		printf("%s cpu usage: %.02f%%\n", curr.name, pct);

		calc_cpu_usage_pct(&curr, &first, &pct);

		printf("%s avg cpu usage: %.02f%%\n", curr.name, pct);
	}

    return 0;
}

